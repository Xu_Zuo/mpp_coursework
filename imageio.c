/*
 * A simple serial solution to the Case Study exercise from the MPP
 * course.  Note that this uses the alternative boundary conditions
 * that are appropriate for the assessed coursework.
 */

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <mpi.h>

#include "pgmio.h"

#define MAXITER   1500
#define PRINTFREQ  100
#define CHECKFREQ  100
#define DELTA      0.1

#define ndims 2

double boundaryval(int i, int m);
double pixelavg(int a, int b, double array[a][b]);

int main (int argc, char **argv){
  /*
    Blocking & Nonblocking communication variables.
  */

  int rank, size, source;
  MPI_Comm comm;
  MPI_Status status[4], status_recv, status_in;
  MPI_Request request[8];

  comm = MPI_COMM_WORLD;

  /*
    2D Cartesian topology variables.
  */

  int comm2d;
  int dims[ndims] = {0,0};
  int period[ndims] = {0,1};  //non-cyclic horizontally, cyclic vertically
  int reorder = 0, disp = 1;

  int left, right, up, down;
  int coords[2];
  int i_lo, j_lo;
  int i_start, j_start;

  MPI_Datatype horr_vector;
  MPI_Datatype decomp_vector;

  double start_time, end_time;
  double start_para, end_para, para_time;

  /*
    Variables for stop criteria.
  */

  double avg_pixel, avg_sum;
  double delta, max_delta, allmax_delta;
  int stop = 0;

  //double masterbuf[M][N];

  int i, j, iter, maxiter;
  int M, N, Mp, Np;
  char *filename;
  double val;

  filename = "edgenew192x128.pgm";
  //filename = "edgenew256x192.pgm";
  //filename = "edgenew512x384.pgm";
  //filename = "edgenew768x768.pgm";

  MPI_Init(NULL,NULL);

  MPI_Comm_size(comm, &size);

  /*
    Define Cartesian topology
  */

  MPI_Dims_create(size,ndims,dims);
  MPI_Cart_create(comm,ndims,dims,period,reorder,&comm2d);
  MPI_Comm_rank(comm2d,&rank);

  MPI_Cart_shift(comm2d,0,disp,&left,&right);
  MPI_Cart_shift(comm2d,1,disp,&up,&down);

  MPI_Cart_coords(comm2d, rank, 2, coords);

  /*
    Define Decomposed array sections used in each processor.
  */
  pgmsize(filename, &M, &N);

  Mp = M/dims[0];
  Np = N/dims[1];

  double masterbuf[M][N];
  double old[Mp+2][Np+2], new[Mp+2][Np+2], edge[Mp+2][Np+2];
  double buf[Mp][Np], temp_buf[Mp][Np];

  /*
     Define verctor of size Mpx1 for horizontal halo swapping.
     The stride size should include halos.
  */

  MPI_Type_vector(Mp, 1, Np+2, MPI_DOUBLE, &horr_vector);
  MPI_Type_commit(&horr_vector);

  MPI_Type_vector(Mp, Np, N, MPI_DOUBLE, &decomp_vector);
  MPI_Type_commit(&decomp_vector);

  //printf("The coordinates of each processor%d are (%d,%d).\n", rank, coords[0],coords[1]);

  /*
     Later decomposed array sections will be sent to rank 0.
     To reassemble array sections on processor 0, Cartesian coordinates
     of each processor are sent to processor 0.
  */

  int coords_x[size], coords_y[size];

  MPI_Gather(&(coords[0]), 1, MPI_INT, &coords_x[0], 1, MPI_INT, 0, comm2d);
	MPI_Gather(&(coords[1]), 1, MPI_INT, &coords_y[0], 1, MPI_INT, 0, comm2d);

  /*
    Read in image on processor 0.
  */

  if(rank == 0){
    printf("Running on %dprocessors\n", size);

    printf("Processing %d x %d image\n", M, N);
    printf("Number of iterations = %d\n", MAXITER);

    printf("\nReading <%s>\n", filename);
    pgmread(filename, masterbuf, M, N);
    printf("\n");
  }

  /*
    File read on processor 0, start timer
  */

  start_time = MPI_Wtime();

  i_lo = Mp*coords[0];
  j_lo = Np*coords[1];

  if(rank==0){
    for(i=0; i<Mp; i++){
      for(j=0; j<Np; j++){
        buf[i][j] = masterbuf[i][j];
      }
    }

    for(source=1; source<size; source++){
      i_start = coords_x[source]*Mp;
      j_start = coords_y[source]*Np;

      MPI_Ssend(&masterbuf[i_start][j_start], 1, decomp_vector, source, 0, comm2d);
      printf("array sections sent to processor%d\n", source);
    }
  }
  else{
    MPI_Recv(&buf[0][0], Mp*Np, MPI_DOUBLE, 0, 0, comm2d, &status_in);
    printf("array sections received on processor%d\n", rank);
  }


  for (i=1;i<Mp+1;i++){
    for (j=1;j<Np+1;j++){
	      edge[i][j]=buf[i-1][j-1];
	  }
  }
  //printf("The value at the boundary of the edge is %f\n", edge[0][0]);

  for (i=0; i<Mp+2;i++){
    for (j=0;j<Np+2;j++){
	     old[i][j]=255.0;
	  }
  }

  //Set fixed boundary conditions on the left and right sides

  for (j=1; j < Np+1; j++){
    //compute sawtooth value

    val = boundaryval((j+j_lo), N);

    old[0][j]   = (int)(255.0*(1.0-val));
    old[Mp+1][j] = (int)(255.0*val);
  }

  start_para = MPI_Wtime();

  for (iter=1;(iter<=MAXITER) && (stop == 0); iter++){
    if(iter%PRINTFREQ==0){
      /*
        Calculate pixel average.
      */
      avg_pixel = pixelavg(Mp+2, Np+2, old);
      MPI_Reduce(&avg_pixel, &avg_sum, 1, MPI_DOUBLE, MPI_SUM, 0, comm2d);
      if(rank == 0){
        printf("\nIteration %d\n", iter);
        printf("The average of pixel values is %f\n", avg_sum/size);
      }
	  }

    //Implement periodic boundary conditions on bottom and top sides
/*
    for (i=1; i < Mp+1; i++){
      old[i][0]   = old[i][Np];
	    old[i][Np+1] = old[i][1];
	  }
*/

    //halo swaps columns
    MPI_Issend(&old[Mp][1], Np, MPI_DOUBLE, right, 0, comm2d, &request[0]);
    MPI_Irecv(&old[0][1], Np, MPI_DOUBLE, left, 0, comm2d, &request[1]);
    //MPI_Recv(&old[0][1], Np, MPI_DOUBLE, left, 0, comm2d, &status[0]);

    MPI_Wait(&request[0], &status[0]);
    MPI_Wait(&request[1], &status[0]);

    MPI_Issend(&old[1][1], Np, MPI_DOUBLE, left, 0, comm2d, &request[2]);
    MPI_Irecv(&old[Mp+1][1], Np, MPI_DOUBLE, right, 0, comm2d, &request[3]);
    //MPI_Recv(&old[Mp+1][1], Np, MPI_DOUBLE, right, 0, comm2d, &status[1]);

    MPI_Wait(&request[2], &status[1]);
    MPI_Wait(&request[3], &status[1]);

    //halo swap rows
    MPI_Issend(&old[1][Np], 1, horr_vector, down, 0, comm2d, &request[4]);
    MPI_Irecv(&old[1][0], 1, horr_vector, up, 0, comm2d, &request[5]);
    //MPI_Recv(&old[1][0], 1, horr_vector, up, 0, comm2d, &status[2]);

    MPI_Wait(&request[4], &status[2]);
    MPI_Wait(&request[5], &status[2]);

    MPI_Issend(&old[1][1], 1, horr_vector, up, 0, comm2d, &request[6]);
    MPI_Irecv(&old[1][Np+1], 1, horr_vector, down, 0, comm2d, &request[7]);
    //MPI_Recv(&old[1][Np+1], 1, horr_vector, down, 0, comm2d, &status[3]);

    MPI_Wait(&request[6], &status[3]);
    MPI_Wait(&request[7], &status[3]);


    for (i=1;i<Mp+1;i++){
      for (j=1;j<Np+1;j++){
	      new[i][j]=0.25*(old[i-1][j]+old[i+1][j]+old[i][j-1]+old[i][j+1]-edge[i][j]);
	    }
	  }

    /*
      Check if the stop criteria have been met.
      Find the maximum delta over the enitre communicator.
    */

    if(iter%CHECKFREQ==0){
      for (i=1;i<Mp+1;i++){
        for (j=1;j<Np+1;j++){
          delta = fabs(new[i][j] - old[i][j]);
          if (delta > max_delta) max_delta = delta;
        }
      }

      MPI_Allreduce(&max_delta, &allmax_delta, 1, MPI_DOUBLE, MPI_MAX, comm2d);
      if(allmax_delta <= DELTA) stop = 1;
      max_delta = 0.0;
      allmax_delta = 0.0;

      //if(rank == 0)
        //printf("Stop criteria checked at iteration%d\n", iter);
    }

    for (i=1;i<Mp+1;i++){
      for (j=1;j<Np+1;j++){
        old[i][j]=new[i][j];
      }
    }

  }

  avg_pixel = pixelavg(Mp+2, Np+2, old);
  MPI_Reduce(&avg_pixel, &avg_sum, 1, MPI_DOUBLE, MPI_SUM, 0, comm2d);

  if(rank == 0){
    printf("\nFinished %d iterations\n", iter-1);
    printf("The average of pixel values is %f\n", avg_sum/size);
    printf("\n");
  }

  end_para = MPI_Wtime();

  for (i=1;i<Mp+1;i++){
    for (j=1;j<Np+1;j++){
      buf[i-1][j-1]=old[i][j];
    }
  }

  //copy decompsed array sections back to masterbuf

  if(rank==0){
    for(i=0; i<Mp; i++){
      for(j=0; j<Np; j++){
        masterbuf[i][j]=buf[i][j];
      }
    }

    for(source=1; source<size; source++){
      i_start = coords_x[source]*Mp;
      j_start = coords_y[source]*Np;

      MPI_Recv(&masterbuf[i_start][j_start], 1, decomp_vector, source, 0, comm2d, &status_recv);
      //printf("array sections received from processor%d\n", source);
      //printf("The starting point of data from processor%d is (%d, %d)\n", rank, i_start, j_start);
    }
  }
  else{
    MPI_Ssend(&buf[0][0], Mp*Np, MPI_DOUBLE, 0, 0, comm2d);
    //printf("array sections sent on processor%d\n", rank);
  }

  /*
    Assembling image finished. End timer
  */

  end_time = MPI_Wtime();

  if(rank == 0){
    printf("\nThe time spent on overall program is %f\n", end_time - start_time);

    para_time = (end_para-start_para)/(iter-1);
    printf("The time spent per iterations is %f\n", para_time);

    filename="imageio192x128.pgm";
    //filename = "imageio256x192.pgm";
    //filename = "imageio512x384.pgm";
    //filename = "imageio768x768.pgm";

    printf("\nWriting <%s>\n", filename);
    pgmwrite(filename, masterbuf, M, N);
  }

  MPI_Finalize();
  return 0;
}

double boundaryval(int i, int m){
  double val;

  val = 2.0*((double)(i-1))/((double)(m-1));
  if (i >= m/2+1) val = 2.0-val;

  return val;
}

/*
  Function which computes the pixel average of a given array.
  Array average = sum of all pixel values/the number of pixels
  Values of halo pixels are not included in calculations.
*/
double pixelavg(int a, int b, double array[a][b]){
  double avg, sum=0;
  int i,j;

  for(i=1; i<(a-1); i++){
    for(j=1; j<(b-1); j++){
      sum += array[i][j];
    }
  }
  avg = sum/((a-1)*(b-1));

  return avg;
}
